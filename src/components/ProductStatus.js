//cap
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ProductStatus({ productid, isActive, getProductData }) {


const setToInactive = () => {
    fetch(`https://capstone2-ecommerceapi-seanandrewcasero.onrender.com/products/${productid}/archive`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (!data) {
          Swal.fire({
            text: 'Product now Inactive',
          });
          getProductData(); 
        } else {
          Swal.fire({
            text: 'error',
          });
        }
      });
  };

  const setToActive = () => {
    fetch(`http://localhost:4000/products/${productid}/activate`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            text: 'Product now Active',
          });
          getProductData(); 
        } else {
          Swal.fire({
            text: 'error',
          });
        }
      });
  };

  return (
    <>
      {isActive ? (
        <Button variant="danger" onClick={setToInactive}>
          Set to Inactive
        </Button>
      ) : (
        <Button variant="success" onClick={setToActive}>
          Set to Active
        </Button>
      )}
    </>
  );
}
