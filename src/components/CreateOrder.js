import Button from 'react-bootstrap/Button';
import Swal from 'sweetalert2';

function CreateOrder({ product, orderQuantity }) {
  const placeOrder = () => {
    const orderData = {
      productID: product._id,
      productName: product.name,
      quantity: orderQuantity,
    };


    fetch('https://capstone2-ecommerceapi-seanandrewcasero.onrender.com/customers/order', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${ localStorage.getItem('token')}`
      },
      body: JSON.stringify(orderData),
    })
      .then((response) => response.json())
      .then((data) => {
        Swal.fire({
          title: 'Order Placed',
          text: 'Your order has been placed successfully.',
          icon: 'success',
        });
      })
      .catch((error) => {
        console.error('Error placing order:', error);
        Swal.fire({
          title: 'Error',
          text: 'An error occurred while placing your order. Please try again.',
          icon: 'error',
        });
      });
  };

  return (
    <Button variant="primary" onClick={placeOrder}>
      Place Order
    </Button>
  );
}

export default CreateOrder;

