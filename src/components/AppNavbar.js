//cap
import { useState, useContext } from 'react';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav'

export default function AppNavbar() {

		const { user } = useContext(UserContext);


	return (
		<Navbar bg= "warning" expand="lg">
			<Container fluid>
				
				<Navbar.Brand as={Link} to="/">
					ReactSuki
				</Navbar.Brand>

				<Navbar.Toggle aria-controls="basic-navbar-nav"/>
				<Navbar.Collapse id='basic-navbar-nav'>
					<Nav className="ms-auto">
						<Nav.Link as={Link} to="/">Home</Nav.Link>
						{!user.isAdmin && (
						<Nav.Link as={Link} to="/catalog">SHOP</Nav.Link>
						)}

						{user.id !== null ? (
							user.isAdmin ? (
						    <>
						    	<Nav.Link as={Link} to="/dashboard">Dashboard</Nav.Link>
						   		<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
							</>
						) : (
							<>
						    	<Nav.Link as={Link} to="/cart">Cart</Nav.Link>
						    	<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
							</>
						)
						) : (
							<>
							<Nav.Link as={Link} to="/login">Login</Nav.Link>
							<Nav.Link as={Link} to="/register">Register</Nav.Link>
							</>
						)}
					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	)
}