//cap
import EditProduct from './EditProduct';
import ProductStatus from './ProductStatus';
import { useState } from 'react';
import { Card, Table, Button} from 'react-bootstrap';

export default function ListProduct({ productlist, getProductData}) {
    

  return (
    <div className="border p-3">
      <Card className="mb-2">
        <Card.Body>
          <Card.Title>Product List</Card.Title>
        </Card.Body>
      </Card>

      {productlist.map((product, index) => (
        <Card key={index} className="mb-1">
          <Card.Body>
            <Card.Title>{product.name}</Card.Title>
            <Card.Subtitle>ID:{product.id}</Card.Subtitle>
            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>{product.description}</Card.Text>
            <Card.Text>Price: P{product.price}</Card.Text>
            < EditProduct product={product} getProductData={getProductData}/>
            <Card.Text>Status: {product.isActive ? 'Active' : 'Inactive'}</Card.Text>
            <ProductStatus  productid={product.id} isActive={product.isActive} getProductData={getProductData}/>
          </Card.Body>
        </Card>
      ))}
    </div>
  );
}
