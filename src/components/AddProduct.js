//cap
import { Card, Button, Form } from 'react-bootstrap';
import UserContext from '../UserContext';
import { useState } from 'react';
import Swal from 'sweetalert2';

export default function AddProduct() {
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");

  function createProduct(e) {
    
    e.preventDefault();

    let token = localStorage.getItem('token');
        console.log(token);

    fetch('https://capstone2-ecommerceapi-seanandrewcasero.onrender.com/products/addproduct',{

            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({

                name: name,
                description: description,
                price: price

            })
        }).then(res => res.json())
        .then(data => {
            if(data){
                Swal.fire({

                    icon:"success",
                    title: "Added to Catalog"

                })

            } else {
                Swal.fire({

                    icon: "error",
                    title: "Error",

                })
            }

        })
    
  }

  return (
    <Card className="mb-3">
      <Card.Body>
        <Card.Title>Add Product</Card.Title>

        <Form onSubmit={createProduct}>
          <Form.Group controlId="productName">
            <Form.Label>Product Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter product name"
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
          </Form.Group>

          <Form.Group controlId="productDescription">
            <Form.Label>Description</Form.Label>
            <Form.Control
              as="textarea"
              rows={3}
              placeholder="Enter product description"
              value={description}
              onChange={(e) => setDescription(e.target.value)}
            />
          </Form.Group>

          <Form.Group controlId="productPrice">
            <Form.Label>Price</Form.Label>
            <Form.Control
              type="number"
              placeholder="Enter product price"
              value={price}
              onChange={(e) => setPrice(e.target.value)}
            />
          </Form.Group>

          <div className="text-center">
            <Button variant="success" type="submit" className="mt-3">Add to Catalog</Button>
          </div>
        </Form>
      </Card.Body>
    </Card>
  );
}

