//cap
import Container from 'react-bootstrap/Container'
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import { useState, useEffect } from 'react';

import AppNavbar from './components/AppNavbar'
import Home from './pages/Home'
import ProductPage from './pages/ProductPage'
import Catalog from './pages/Catalog'
import Register from './pages/Register'
import Login from './pages/Login';
import Logout from './pages/Logout';
import Dashboard from './pages/Dashboard';
import Error from './pages/Error';

import './App.css';
import { UserProvider } from './UserContext';

function App() {
  
  

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }
  
  useEffect(() => {
    const token = localStorage.getItem('token');

    if (token) {
        fetch(`https://capstone2-ecommerceapi-seanandrewcasero.onrender.com/customers/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if (typeof data._id !== "undefined") {
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                });
            } else {
                setUser({
                    id: null,
                    isAdmin: null
                });
            }
        });
    }
}, []);
  
  return (
    <UserProvider value={{ user, setUser, unsetUser}}>
    <Router>
      
      <AppNavbar />
      <Container fluid>

        <Routes>

            <Route path="/" element={<Home/>} />
            {/* <Route path="/profile" element={<Profile/>} />*/}
            <Route path="/catalog" element={<Catalog/>} />
            <Route exact path="/catalog/:productId" element={<ProductPage/>} />
            <Route path="/register" element={<Register/>} />
            <Route path="/dashboard" element={<Dashboard/>} />
            <Route path="/login" element={<Login/>} />
            <Route path="/logout" element={<Logout/>} />
            <Route path="*" element={<Error/>} />

        </Routes>

      </Container>
    </Router> 
    </UserProvider>
  );
}

export default App;

