import { useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext';
import { Card, Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Products() {
  const { user } = useContext(UserContext);
  const [products, setProducts] = useState([]);

  const getProductData = () => {
    fetch(`https://capstone2-ecommerceapi-seanandrewcasero.onrender.com/products/listproduct`)
      .then(res => res.json())
      .then(data => {
        setProducts(data);
      });
  };

  useEffect(() => {
    getProductData();
  }, []);

  return (
    <div className="border p-3">
      <Card className="mb-2">
        <Card.Body className="text-center">
          <Card.Title>Catalogue</Card.Title>
        </Card.Body>
      </Card>
      <Row>
        {products.map((product, index) => (
          <Col key={index} md={4} className="mb-3"> 
            <Card>
              <Card.Body>
                <Card.Title>{product.name}</Card.Title>
                <Card.Subtitle>ID: {product.id}</Card.Subtitle>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{product.description}</Card.Text>
                <Card.Text>Price: P{product.price}</Card.Text>
                <Card.Text>Status: {product.isActive ? 'On sale' : 'Not Available'}</Card.Text>
                  <Link to={`/catalog/${product.id}`}>
                  <Button variant="primary">View Product</Button>
                </Link>
              </Card.Body>
            </Card>
          </Col>
        ))}
      </Row>
    </div>
  );
}