//cap
import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register(){

	const {user} = useContext(UserContext);

	const [name, setName ] = useState("");
    const [email,setEmail] = useState("");
    const [password,setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [isActive, setIsActive] = useState(false);


	function registerCustomer(e){
    	e.preventDefault();
    	fetch('https://capstone2-ecommerceapi-seanandrewcasero.onrender.com/customers/register',{
    		method: 'POST',
    		headers:{
    			"Content-Type":"application/json"
    		},
    		body: JSON.stringify({

    			name: name,
    			email: email,
    			password: password
    		})
    	})
    	.then(res=>res.json())
    	.then(data=>{
    		if(data){

    			setName('');
    			setEmail('');
    			setPassword('');
    			setConfirmPassword('')

    			Swal.fire({
					title: "Success!",
					icon: 'success',
					text: "Details registered!."
				})

    		}else{
    			Swal.fire({
					title: "Something went wrong",
					icon: 'error',
					text: "email in use."
				})
    		}
    	})
    }


    useEffect(()=>{

    	if((email !=="" && password !=="" && confirmPassword !== "") && (password === confirmPassword)){

    		setIsActive(true)

    	}else{
    		setIsActive(false)
    	}

    },[email,password,confirmPassword])

    return(
		(user.id !== null) ?
			<Navigate to="/" />
		:
		<Form onSubmit={(e)=>registerCustomer(e)}>
			<h1 className="my-5 text-center">Register</h1>

			<Form.Group>
				<Form.Label>Name:</Form.Label>
				<Form.Control
					type="name"
					placeholder="Enter Name"
					required
					value={name}
					onChange={e=>{setName(e.target.value)}}
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Email:</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter Email"
					required
					value={email}
					onChange={e=>{setEmail(e.target.value)}}
				/>
			</Form.Group>

			

			<Form.Group>
				<Form.Label>Password: </Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter Password"
					required
					value={password}
					onChange={e=>{setPassword(e.target.value)}}
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Confirm Password: </Form.Label>
				<Form.Control
					type="password"
					placeholder="Confirm Password"
					required
					value={confirmPassword}
					onChange={e=>{setConfirmPassword(e.target.value)}}
				/>
			</Form.Group>

			{
				isActive ?
				<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
				:
				<Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
			}

		</Form>
	)
}



