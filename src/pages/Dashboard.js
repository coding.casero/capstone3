//cap
import {useState,useEffect, useContext} from 'react';
import { Container, Row, Col, Card } from 'react-bootstrap';
import UserContext from '../UserContext';
import AddProduct from '../components/AddProduct';
import ListProduct from '../components/ListProduct';


const Dashboard = () => {
  const { user } = useContext(UserContext);

  const [productlist, setProductList] = useState([]);
  const [dataFetched, setDataFetched] = useState(false);

  const getProductData = () => {
    fetch('https://capstone2-ecommerceapi-seanandrewcasero.onrender.com/products/listproduct')
      .then((response) => {
        return response.json();
      })
      .then((adminData) => {
        setProductList(adminData);
        setDataFetched(true); 
      })
      .catch((error) => {
        console.error('Error fetching data:', error);
      });
  };

  useEffect(() => {
    if (user && user.isAdmin && !dataFetched) {
      getProductData();
    }
  }, [user, dataFetched]); 

  return (
    <>
      {user && user.isAdmin ? (
        <>
          <h1>Admin Dashboard</h1>
          <AddProduct />
          <ListProduct productlist={productlist}  getProductData={getProductData}/>
        </>
      ) : (
        <Container>
          <Row className="justify-content-center mt-5">
            <Col xs={12} md={6}>
              <div className="text-center text-danger">
                <h1>You are not authorized to access this page.</h1>
              </div>
            </Col>
          </Row>
        </Container>
      )}
    </>
  );
};

export default Dashboard;