import { useEffect, useState, useContext } from 'react';
import { useParams, NavLink } from 'react-router-dom';
import { Card, Button, Modal, Form } from 'react-bootstrap';
import UserContext from '../UserContext';
import CreateOrder from '../components/CreateOrder';

export default function ProductPage() {
  const { productId } = useParams();
  const [product, setProduct] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const [orderQuantity, setOrderQuantity] = useState(1);
  const { user } = useContext(UserContext);

  useEffect(() => {
    fetch(`https://capstone2-ecommerceapi-seanandrewcasero.onrender.com/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        setProduct(data);
      });
  }, [productId]);

  if (!product) {
    return <div>Loading...</div>;
  }

  const handleOrderClick = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const handleQuantityChange = (e) => {
    // Update orderQuantity when the user changes the quantity
    const quantity = parseInt(e.target.value, 10);
    if (!isNaN(quantity) && quantity >= 1) {
      setOrderQuantity(quantity);
    }
  };

  return (
    <div className="border p-3">
      <Card>
        <Card.Body>
          <Card.Title>{product.name}</Card.Title>
          <Card.Subtitle>ID: {product._id}</Card.Subtitle>
          <Card.Subtitle>Description:</Card.Subtitle>
          <Card.Text>{product.description}</Card.Text>
          <Card.Text>Price: P{product.price}</Card.Text>
          <Card.Text>Status: {product.isActive ? 'On sale' : 'Not Available'}</Card.Text>
          {product.isActive && !user.isAdmin ? (
            user.id !== null ? (
              <Button variant="primary" onClick={handleOrderClick}>
                Order
              </Button>
            ) : (
              <NavLink to="/login">
                <Button variant="primary">Log in to Order</Button>
              </NavLink>
            )
          ) : (
            <p>This product is not available for order.</p>
          )}
        </Card.Body>
      </Card>

      <Modal show={showModal} onHide={handleCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>Order Confirmation</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>Product Name: {product.name}</p>
          <p>Product ID: {product._id}</p>
          {product.isActive && !user.isAdmin && (
            <Form>
              <Form.Group controlId="quantity">
                <Form.Label>Quantity</Form.Label>
                <Form.Control
                  type="number"
                  placeholder="Enter quantity"
                  value={orderQuantity} // Display the orderQuantity in the input
                  onChange={handleQuantityChange} // Update orderQuantity when the input changes
                />
              </Form.Group>
            </Form>
          )}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseModal}>
            Close
          </Button>
          {/* Pass the orderQuantity as a prop to CreateOrder */}
          <CreateOrder product={product} orderQuantity={orderQuantity} />
        </Modal.Footer>
      </Modal>
    </div>
  );
}
