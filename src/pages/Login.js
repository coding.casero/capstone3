//cap
import React, { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'; 
import { Navigate } from 'react-router-dom';

export default function Login() {
    const { user, setUser } = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(true);

    const retrieveUserDetails = (token) => {
        fetch('https://capstone2-ecommerceapi-seanandrewcasero.onrender.com/customers/details', {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
            .then((res) => res.json())
            .then((data) => {
                console.log(data);

                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin,
                });

            });
    };

    const authenticate = (e) => {
        e.preventDefault();
        fetch('https://capstone2-ecommerceapi-seanandrewcasero.onrender.com/customers/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: email,
                password: password,
            }),
        }).then((res) => res.json()).then((data) => {
                                        console.log(data)
                                        if (typeof data.access !== 'undefined') {
                                            localStorage.setItem('token', data.access);
                                            retrieveUserDetails(data.access);
                                            Swal.fire({
                                                title: 'Login successful',
                                                icon: 'success',
                                                text: 'Welcome to ReactSuki!',
                                            });
                                        } else {
    
                                            Swal.fire({
                                                title: 'Login failed',
                                                icon: 'error',
                                                text: "Password doesn't match or there was an issue with the server.",
                                            });
                                        }
                                    });
            
    };

    useEffect(() => {
        if (email !== '' && password !== '') {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [email, password]);

    return user.id !== null ? (
        <Navigate to="/" />
    ) : (
        <Form onSubmit={(e) => authenticate(e)}>
            <h1 className="my-5 text-center">Login</h1>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control
                    type="email"
                    placeholder="Enter email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control
                    type="password"
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

            {isActive ? (
                <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                </Button>
            ) : (
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            )}
        </Form>
    );
}

